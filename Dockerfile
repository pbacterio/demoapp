FROM scratch
ARG TARGETARCH
ADD build/${TARGETARCH}/demoapp /demoapp
ENTRYPOINT ["/demoapp"]
