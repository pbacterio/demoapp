#!/bin/sh

CI_REGISTRY_IMAGE=${CI_REGISTRY_IMAGE:-registry.gitlab.com/pbacterio/demoapp}

if [ "$1" = "clean" ]
then
    set -ex
    rm -rf build
    exit
fi

if [ "$1" = "image" ]
then
    set -ex
    docker buildx create --use
    docker buildx build --platform linux/arm/v7,linux/arm64/v8,linux/amd64 --tag ${CI_REGISTRY_IMAGE} --push .
    docker buildx rm
    exit
fi

set -ex
for ARCH in amd64 arm64 arm; do
    CGO_ENABLED=0 GOARCH=${ARCH} go build -o build/${ARCH}/demoapp -ldflags="-s -w"
    upx -q -1 build/${ARCH}/demoapp
done
